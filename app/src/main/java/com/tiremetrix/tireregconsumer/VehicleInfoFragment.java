package com.tiremetrix.tireregconsumer;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

/**
 * Created by sholliday on 11/17/15.
 */
public class VehicleInfoFragment extends Fragment {
    MainActivity act;
    EditText txtVIN;
    Spinner spnYears;
    Spinner spnMakes;
    Spinner spnModels;
    Button btnDone;
    ArrayList<Year> years;
    ArrayList<Make> makes;
    ArrayList<Model> models;
    Year selectedYear = null;
    Make selectedMake = null;
    Model selectedModel= null;
    InputMethodManager imm;
    String replaceVIN;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        act = (MainActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_vehicle_info, container, false);
        txtVIN = (EditText)view.findViewById(R.id.txtVIN);
        txtVIN.setSingleLine(true);
        txtVIN.setOnFocusChangeListener(clkTxtVIN);
        txtVIN.addTextChangedListener(txtVINWatcher);
        txtVIN.setOnEditorActionListener(txtVINEdit);
        ImageButton btnCamera = (ImageButton)view.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(clkScan);
        years = act.dbHelper.getAllYears();
        ArrayList<String> yearValues = new ArrayList<String>();
        for(Year year : years){
            if(year.ID == 0){
                yearValues.add("Select Year");
            }else{
                yearValues.add(Integer.toString(year.ID));
            }
        }
        spnYears = (Spinner)view.findViewById(R.id.spnYears);
        ArrayAdapter<String> ada = new ArrayAdapter<String>(act, android.R.layout.simple_spinner_item, yearValues);
        ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnYears.setAdapter(ada);
        spnYears.setOnItemSelectedListener(clkYear);
        spnMakes = (Spinner)view.findViewById(R.id.spnMakes);
        spnModels = (Spinner)view.findViewById(R.id.spnModels);
        btnDone = (Button)view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(clkDone);
        imm = (InputMethodManager)act.getSystemService(act.INPUT_METHOD_SERVICE);
        return view;
    }
    View.OnFocusChangeListener clkTxtVIN = new View.OnFocusChangeListener(){
        @Override
        public void onFocusChange(View v, boolean hasFocus){
            if(!hasFocus){
                txtVIN.setText(replaceVIN);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    };
    TextWatcher txtVINWatcher = new TextWatcher(){
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after){
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count){
        }
        @Override
        public void afterTextChanged(Editable s){
            replaceVIN = txtVIN.getText().toString().toUpperCase();
            replaceVIN = replaceVIN.replace('G', '6');
            replaceVIN = replaceVIN.replace('I', '1');
            replaceVIN = replaceVIN.replace('O', '0');
            replaceVIN = replaceVIN.replace('Q', '0');
            replaceVIN = replaceVIN.replace('S', '5');
            replaceVIN = replaceVIN.replace('Z', '2');
        }
    };
    TextView.OnEditorActionListener txtVINEdit = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
            if(actionId == EditorInfo.IME_ACTION_NEXT){
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                txtVIN.setText(replaceVIN);
                return true;
            }
            return false;
        }
    };
    View.OnClickListener clkScan = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            IntentIntegrator.initiateScan(act);
        }
    };
    AdapterView.OnItemSelectedListener clkYear = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            imm.hideSoftInputFromWindow(parentView.getWindowToken(), 0);
            if(position > 0){
                txtVIN.setText("");
                selectedYear = years.get(position);
                makes = act.dbHelper.getMakesByYear(selectedYear.ID);
                ArrayList<String> makeValues = new ArrayList<String>();
                for(Make make : makes){
                    makeValues.add(make.make);
                }
                ArrayAdapter<String> ada = new ArrayAdapter<String>(act, android.R.layout.simple_spinner_item, makeValues);
                ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnMakes.setAdapter(ada);
                spnMakes.setOnItemSelectedListener(clkMake);
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    AdapterView.OnItemSelectedListener clkMake = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            txtVIN.setText("");
            selectedMake = makes.get(position);
            models = act.dbHelper.getModelsByYearAndMake(selectedYear.ID, selectedMake);
            ArrayList<String> modelValues = new ArrayList<String>();
            for(Model model : models){
                modelValues.add(model.model);
            }
            ArrayAdapter<String> ada = new ArrayAdapter<String>(act, android.R.layout.simple_spinner_item, modelValues);
            ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnModels.setAdapter(ada);
            spnModels.setOnItemSelectedListener(clkModel);
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    AdapterView.OnItemSelectedListener clkModel = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            txtVIN.setText("");
            selectedModel = models.get(position);
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        switch(requestCode){
            default:
                IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
                if(scanResult != null){
                    if(scanResult.getContents() != null){
                        txtVIN.setText(scanResult.getContents().toString());
                    }
                }else{
                    Toast.makeText(act, "Your scan was unsuccessful.\nPlease try again.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
    View.OnClickListener clkDone = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            act.closeFrags();
        }
    };
}
