package com.tiremetrix.tireregconsumer;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper{
    private static String DB_PATH = "/data/data/com.tiremetrix.tireregconsumer/databases/";
    private static String DB_NAME = "tpms.sqlite";
    private SQLiteDatabase db;
    private final Context ctx;
    public DBHelper(Context context){
        super(context, DB_NAME, null, 1);
        this.ctx = context;
    }
    public void createDataBase() throws IOException{
        //boolean dbExist = checkDataBase();
        //if(!dbExist){
            this.getReadableDatabase();
            try{
                copyDataBase();
            }catch(IOException e){
                throw new Error("Error copying database");
            }
        //}
    }
    private boolean checkDataBase(){
        SQLiteDatabase checkDB = null;
        try{
            String dbPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READONLY);
        }catch(SQLException e){
        }
        if(checkDB != null){
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }
    private void copyDataBase() throws IOException{
        InputStream input = ctx.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream output = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = input.read(buffer))>0){
            output.write(buffer, 0, length);
        }
        output.flush();
        output.close();
        input.close();
    }
    public void openDataBase() throws SQLException {
        String dbPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READONLY);
    }
    @Override
    public synchronized void close() {
        if(db != null) {
            db.close();
        }
        super.close();
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
    public ArrayList<Year> getAllYears(){
        ArrayList<Year> years = new ArrayList<Year>();
        Year yearZero = new Year();
        yearZero.ID = 0;
        years.add(yearZero);
        try{
            Cursor crs = db.rawQuery("SELECT DISTINCT Year FROM ymm ORDER BY Year ASC", null);
            while(crs.moveToNext()){
                Year year = new Year();
                year.ID = crs.getInt(0);
                years.add(year);
            }
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return years;
    }
    public ArrayList<Make> getMakesByYear(int yearID){
        ArrayList<Make> makes = new ArrayList<Make>();
        try{
            Cursor crs = db.rawQuery("SELECT DISTINCT m.ID, m.Make FROM makes m JOIN ymm y ON m.ID = y.MakeID WHERE y.Year = ? ORDER BY m.Make ASC", new String[] {Integer.toString(yearID)});
            while(crs.moveToNext()){
                Make make = new Make();
                make.ID = crs.getInt(0);
                make.make = crs.getString(1);
                makes.add(make);
            }
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return makes;
    }
    public ArrayList<Model> getModelsByYearAndMake(int yearID, Make make){
        ArrayList<Model> models = new ArrayList<Model>();
        try{
            Cursor crs = db.rawQuery("SELECT DISTINCT m.ID, m.Model FROM models m JOIN ymm y ON m.ID = y.ModelID WHERE y.Year = ? AND y.MakeID = ? ORDER BY m.Model ASC", new String[] {Integer.toString(yearID), Integer.toString(make.ID)});
            while(crs.moveToNext()){
                Model model = new Model();
                model.ID = crs.getInt(0);
                model.model = crs.getString(1);
                models.add(model);
            }
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return models;
    }
    public ArrayList<String> getAllTireMakes(){
        ArrayList<String> makes = new ArrayList<String>();
        makes.add("Select Brand");
        try{
            Cursor crs = db.rawQuery("SELECT DISTINCT Make FROM tires ORDER BY Make ASC", null);
            while(crs.moveToNext()){
                String make = crs.getString(0);
                makes.add(make);
            }
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return makes;
    }
    public ArrayList<String> getTireModelsByMake(String make){
        ArrayList<String> models = new ArrayList<String>();
        models.add("Select Model");
        try{
            Cursor crs = db.rawQuery("SELECT DISTINCT Model FROM tires WHERE Make = ? ORDER BY Model ASC", new String[] {make});
            while(crs.moveToNext()){
                String model = crs.getString(0);
                models.add(model);
            }
        }catch(SQLiteException e){
            Log.d("sql", e.getLocalizedMessage());
        }
        return models;
    }
}