package com.tiremetrix.tireregconsumer;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by sholliday on 11/16/15.
 */
public class SplashActivity extends Activity {
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash);
    }
    @Override
    public void onStart(){
        super.onStart();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                finish();
            }
        }, 2000);
    }
}
