package com.tiremetrix.tireregconsumer;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by sholliday on 11/17/15.
 */
public class TireInfoFragment extends Fragment {
    MainActivity act;
    LayoutInflater inflater;
    LinearLayout layTires;
    ArrayList<Tire> tires;
    InputMethodManager imm;
    EditText currTIN;
    Spinner spnModels;
    ArrayList makes;
    String selectedMake;
    ArrayList models;
    String selectedModel;
    boolean tinHasChanged;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        act = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_tire_info, container, false);
        layTires = (LinearLayout)view.findViewById(R.id.layTires);
        tires = new ArrayList<Tire>();
        for(int i = 0; i < 4; i++){
            Tire tire = new Tire();
            tires.add(tire);
        }
        setupList();
        imm = (InputMethodManager)act.getSystemService(act.INPUT_METHOD_SERVICE);
        return view;
    }
    public void setupList(){
        layTires.removeAllViews();
        for(int i = 0; i < tires.size(); i++){
            Tire tire = tires.get(i);
            inflater = (LayoutInflater)act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View cell = inflater.inflate(R.layout.cell_tire, null);
            cell.setTag(i);
            ImageButton btnCopy = (ImageButton)cell.findViewById(R.id.btnCopy);
            if(i > 0){
                btnCopy.setTag(i);
                btnCopy.setOnClickListener(clkCopy);
            }else{
                btnCopy.setVisibility(View.INVISIBLE);
            }
            EditText txtTIN = (EditText)cell.findViewById(R.id.txtTIN);
            txtTIN.setTag(i);
            txtTIN.setHint("DOT CODE #" + i);
            txtTIN.setText(tire.TIN);
            txtTIN.setOnFocusChangeListener(clkTxtTIN);
            txtTIN.addTextChangedListener(txtTINWatcher);
            txtTIN.setOnEditorActionListener(txtTINEdit);
            TextView txtMakeAndModel = (TextView)cell.findViewById(R.id.txtMakeAndModel);
            if((tire.Make != null) && (tire.Model != null)){
                txtMakeAndModel.setText(tire.Make + " " + tire.Model);
            }
            ImageButton btnCamera = (ImageButton)cell.findViewById(R.id.btnCamera);
            //btnCamera.setOnClickListener(clkCamera);
            layTires.addView(cell);
        }
        View cell = inflater.inflate(R.layout.cell_new_tire, null);
        ImageButton btnAdd = (ImageButton)cell.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(clkAdd);
        layTires.addView(cell);
        Button btnDone = new Button(act);
        btnDone.setText("Done");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(8, 0, 8, 2);
        btnDone.setLayoutParams(params);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            btnDone.setBackgroundColor(getResources().getColor(R.color.colorAccent, null));
        }else{
            btnDone.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
        btnDone.setTextColor(Color.WHITE);
        btnDone.setOnClickListener(clkDone);
        layTires.addView(btnDone);
    }
    View.OnFocusChangeListener clkTxtTIN = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus){
            if(hasFocus){
                currTIN = (EditText)v;
                tinHasChanged = false;
            }else{
                if(tinHasChanged){
                    Tire tire = tires.get((int)v.getTag());
                    EditText txt = (EditText)v;
                    txt.setText(tire.TIN);
                    checkTireTIN(v);
                }
            }
        }
    };
    TextWatcher txtTINWatcher = new TextWatcher(){
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after){
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count){
        }
        @Override
        public void afterTextChanged(Editable s){
            tinHasChanged = true;
            String replaceTIN = currTIN.getText().toString().toUpperCase();
            replaceTIN = replaceTIN.replace('G', '6');
            replaceTIN = replaceTIN.replace('I', '1');
            replaceTIN = replaceTIN.replace('O', '0');
            replaceTIN = replaceTIN.replace('Q', '0');
            replaceTIN = replaceTIN.replace('S', '5');
            replaceTIN = replaceTIN.replace('Z', '2');
            Tire tire = tires.get((int)currTIN.getTag());
            tire.TIN = replaceTIN;
        }
    };
    TextView.OnEditorActionListener txtTINEdit = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
            if(actionId == EditorInfo.IME_ACTION_NEXT){
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    };
    View.OnClickListener clkCopy = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            int position = (int)v.getTag();
            Tire tire = tires.get(position - 1);
            tires.set(position, tire);
            setupList();
        }
    };
    View.OnClickListener clkAdd = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            Tire tire = new Tire();
            tires.add(tire);
            setupList();
        }
    };
    public void checkTireTIN(View v){
        final EditText txt = (EditText)v;
        Tire tire = tires.get((int)txt.getTag());
        if(tire.TIN.length() > 5){
            String firstTwo = tire.TIN.substring(0, 2);
            boolean plantFound = false;
            if(firstTwo.equals("HP")){
                plantFound = true;
            }else if(firstTwo.equals("HP")){
                plantFound = true;
            }else if(firstTwo.equals("FH")){
                plantFound = true;
            }else if(firstTwo.equals("FJ")){
                plantFound = true;
            }else if(firstTwo.equals("FK")){
                plantFound = true;
            }else if(firstTwo.equals("FL")){
                plantFound = true;
            }else if(firstTwo.equals("4M")){
                plantFound = true;
            }else if(firstTwo.equals("B6")){
                plantFound = true;
            }else if(firstTwo.equals("M3")){
                plantFound = true;
            }
            if(!plantFound){
                AlertDialog.Builder builder = new AlertDialog.Builder(act);
                builder.setTitle("Invalid DOT Code");
                builder.setMessage("Your DOT code must begin with HP, FH, FH, FK, FL, 4M, B6, or M3.");
                builder.setPositiveButton("OK", null);
                builder.show();
                return;
            }
            int week = Integer.parseInt(tire.TIN.substring(tire.TIN.length() - 4, tire.TIN.length() - 2));
            if((week < 1) || (week > 52)){
                AlertDialog.Builder builder = new AlertDialog.Builder(act);
                builder.setTitle("Invalid DOT Code");
                builder.setMessage("The fourth and third from last digits in your DOT code must be between 01 and 52.");
                builder.setPositiveButton("OK", null);
                builder.show();
                return;
            }
            int year = Integer.parseInt(tire.TIN.substring(tire.TIN.length() - 2, tire.TIN.length()));
            if((year < 0) || (year > 16)){
                AlertDialog.Builder builder = new AlertDialog.Builder(act);
                builder.setTitle("Invalid DOT Code");
                builder.setMessage("The last two digits in your DOT code must be between 00 and 16.");
                builder.setPositiveButton("OK", null);
                builder.show();
                return;
            }
            selectedMake = null;
            selectedModel = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(act);
            builder.setTitle("Choose Tire Brand and Model");
            inflater = (LayoutInflater)act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialog = inflater.inflate(R.layout.dialog_tire_select, null);
            Spinner spnMakes = (Spinner)dialog.findViewById(R.id.spnMakes);
            makes = act.dbHelper.getAllTireMakes();
            ArrayAdapter<String> ada = new ArrayAdapter<String>(act, android.R.layout.simple_spinner_item, makes);
            ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnMakes.setAdapter(ada);
            spnMakes.setOnItemSelectedListener(clkMake);
            spnModels = (Spinner)dialog.findViewById(R.id.spnModels);
            builder.setView(dialog);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which){
                    if((selectedMake != null) && (selectedModel != null)){
                        Tire tire = tires.get((int)txt.getTag());
                        tire.Make = selectedMake;
                        tire.Model = selectedModel;
                        setupList();
                    }
                }
            });
            builder.show();
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(act);
            builder.setTitle("Invalid DOT Code");
            builder.setMessage("Your DOT Code isn't long enough.");
            builder.setPositiveButton("OK", null);
            builder.show();
        }
    }
    View.OnClickListener clkDone = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            act.closeFrags();
        }
    };
    AdapterView.OnItemSelectedListener clkMake = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            if(position > 0){
                selectedMake = makes.get(position).toString();
                models = act.dbHelper.getTireModelsByMake(selectedMake);
                ArrayAdapter<String> ada = new ArrayAdapter<String>(act, android.R.layout.simple_spinner_item, models);
                ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnModels.setAdapter(ada);
                spnModels.setOnItemSelectedListener(clkModel);
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
    AdapterView.OnItemSelectedListener clkModel = new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            if(position > 0){
                selectedModel = models.get(position).toString();
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
        }
    };
}
