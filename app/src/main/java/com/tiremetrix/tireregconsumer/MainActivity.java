package com.tiremetrix.tireregconsumer;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    DBHelper dbHelper;
    ImageView imvBanner;
    LinearLayout layFrags;
    EditText txtName;
    EditText txtEmail;
    EditText txtPhone;
    EditText txtCountry;
    EditText txtAddressOne;
    EditText txtAddressTwo;
    EditText txtCity;
    EditText txtState;
    EditText txtZip;
    Button btnAutoFill;
    Button btnUseMap;
    Button btnDealerInfo;
    Button btnVehicleInfo;
    Button btnTireInfo;
    Button btnSubmit;
    LocationManager locMgr;
    ProgressDialog prgDialog;
    GoogleApiClient apiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent iSplash = new Intent(MainActivity.this, SplashActivity.class);
        //startActivity(iSplash);
        dbHelper = new DBHelper(this);
        try{
            dbHelper.createDataBase();
        }catch(IOException ioe){
            throw new Error("Unable to create database");
        }
        try{
            dbHelper.openDataBase();
        }catch(SQLException e){
            throw e;
        }
        imvBanner = (ImageView)findViewById(R.id.imvBanner);
        InputStream open = null;
        try{
            open = this.getAssets().open("banner.jpg");
            imvBanner.setScaleType(ImageView.ScaleType.FIT_XY);
            imvBanner.setImageBitmap(BitmapFactory.decodeStream(open));
        }catch(IOException e){
            e.printStackTrace();
        }
        layFrags = (LinearLayout)findViewById(R.id.layFrags);
        txtName = (EditText) findViewById(R.id.txtName);
        txtName.setSingleLine(true);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtEmail.setSingleLine(true);
        txtPhone = (EditText) findViewById(R.id.txtPhone);
        txtPhone.setSingleLine(true);
        txtCountry = (EditText) findViewById(R.id.txtCountry);
        txtCountry.setSingleLine(true);
        txtAddressOne = (EditText) findViewById(R.id.txtAddressOne);
        txtAddressOne.setSingleLine(true);
        txtAddressTwo = (EditText) findViewById(R.id.txtAddressTwo);
        txtAddressTwo.setSingleLine(true);
        txtCity = (EditText) findViewById(R.id.txtCity);
        txtCity.setSingleLine(true);
        txtState = (EditText) findViewById(R.id.txtState);
        txtState.setSingleLine(true);
        txtZip = (EditText) findViewById(R.id.txtZip);
        txtZip.setSingleLine(true);
        btnAutoFill = (Button)findViewById(R.id.btnAutoFill);
        btnAutoFill.setOnClickListener(clkAutoFill);
        btnUseMap = (Button)findViewById(R.id.btnUseMap);
        btnUseMap.setOnClickListener(clkUseMap);
        btnDealerInfo = (Button)findViewById(R.id.btnDealerInfo);
        btnDealerInfo.setOnClickListener(clkDealerInfo);
        btnVehicleInfo = (Button)findViewById(R.id.btnVehicleInfo);
        btnVehicleInfo.setOnClickListener(clkVehicleInfo);
        btnTireInfo = (Button)findViewById(R.id.btnTireInfo);
        btnTireInfo.setOnClickListener(clkTireInfo);
        apiClient = new GoogleApiClient
                .Builder(MainActivity.this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }
    View.OnClickListener clkAutoFill = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(locMgr == null){
                locMgr = (LocationManager)MainActivity.this.getSystemService(Context.LOCATION_SERVICE);
            }
            prgDialog = new ProgressDialog(MainActivity.this);
            prgDialog.setMessage("Getting Location...");
            prgDialog.show();
            requestLocation();
        }
    };
    View.OnClickListener clkUseMap = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(apiClient == null || !apiClient.isConnected()){
                Toast.makeText(MainActivity.this, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            }
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            try{
                startActivityForResult(builder.build(MainActivity.this), 0);
            }catch(GooglePlayServicesRepairableException e){
                Toast.makeText(MainActivity.this, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
            }catch( GooglePlayServicesNotAvailableException e ) {
                Toast.makeText(MainActivity.this, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
            }
        }
    };
    View.OnClickListener clkDealerInfo = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment = new DealerInfoFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.layFrags, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            layFrags.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        }
    };
    View.OnClickListener clkVehicleInfo = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment = new VehicleInfoFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.layFrags, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            layFrags.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        }
    };
    View.OnClickListener clkTireInfo = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment = new TireInfoFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.layFrags, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            layFrags.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        }
    };
    public void requestLocation(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                return;
            }
        }
        ConnectivityManager conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if(netInfo != null){
            if(netInfo.isConnected()) {
                locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locListener);
            }else{
                locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
            }
        }else{
            locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode){
            case 0: //fine location
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocation();
                }
                break;
        }
    }
    LocationListener locListener = new LocationListener(){
        public void onLocationChanged(Location location){

            try {
                Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    Address address = addresses.get(0);
                    txtCountry.setText(address.getCountryCode());
                    txtAddressOne.setText(address.getAddressLine(0));
                    txtCity.setText(address.getLocality());
                    txtState.setText(address.getAdminArea().substring(0, 2).toUpperCase());
                    txtZip.setText(address.getPostalCode());
                } else {
                    Toast.makeText(MainActivity.this, "There was an error retrieving your location. Please try again later.", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                Toast.makeText(MainActivity.this, "There was an error retrieving your location. Please try again later.", Toast.LENGTH_SHORT).show();
            } catch (IllegalArgumentException e) {
                Toast.makeText(MainActivity.this, "There was an error retrieving your location. Please try again later.", Toast.LENGTH_SHORT).show();
            }
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                    return;
                }
            }
            locMgr.removeUpdates(locListener);
            prgDialog.dismiss();
        }
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
        public void onProviderEnabled(String provider) {
        }
        public void onProviderDisabled(String provider) {
        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 0 && resultCode == Activity.RESULT_OK){ //place
            Place place = PlacePicker.getPlace(data, this);
            if(place == null) {
                return;
            }
            //txtName.setText(place.getName());
            String[] addParts = place.getAddress().toString().split(", ");
            String street = addParts[0];
            String city = addParts[1];
            String[] stateZip = addParts[2].split(" ");
            String state = stateZip[0];
            String zip = stateZip[1];
            String country = addParts[3];
            txtPhone.setText(place.getPhoneNumber().toString().replace("+", ""));
            txtCountry.setText(country);
            txtAddressOne.setText(street);
            txtCity.setText(city);
            txtState.setText(state);
            txtZip.setText(zip);
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    public void closeFrags(){
        while(getFragmentManager().getBackStackEntryCount() > 0){
            getFragmentManager().popBackStackImmediate();
        }
        layFrags.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    }
    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if(count == 0){
            super.onBackPressed();
        }else{
            getFragmentManager().popBackStack();
            if(count == 1){
                closeFrags();
            }
        }
    }
    @Override
    protected void onStart(){
        super.onStart();
        if(apiClient != null){
            apiClient.connect();
        }
    }
    @Override
    protected void onStop(){
        if(apiClient != null && apiClient.isConnected()){
            apiClient.disconnect();
        }
        super.onStop();
    }
    @Override
    public void onConnected(Bundle bundle){
    }
    @Override
    public void onConnectionSuspended(int i){
    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult){
    }
}
