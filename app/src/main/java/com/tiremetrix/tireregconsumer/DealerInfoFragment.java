package com.tiremetrix.tireregconsumer;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by sholliday on 11/17/15.
 */
public class DealerInfoFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    MainActivity act;
    EditText txtDealerName;
    EditText txtEmail;
    EditText txtPhone;
    EditText txtCountry;
    EditText txtAddressOne;
    EditText txtAddressTwo;
    EditText txtCity;
    EditText txtState;
    EditText txtZip;
    Button btnAutoFill;
    Button btnUseMap;
    Button btnUseZip;
    Button btnDone;
    LocationManager locMgr;
    ProgressDialog prgDialog;
    GoogleApiClient apiClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        act = (MainActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_dealer_info, container, false);
        txtDealerName = (EditText)view.findViewById(R.id.txtDealerName);
        txtDealerName.setSingleLine(true);
        txtEmail = (EditText)view.findViewById(R.id.txtEmail);
        txtEmail.setSingleLine(true);
        txtPhone = (EditText)view.findViewById(R.id.txtPhone);
        txtPhone.setSingleLine(true);
        txtCountry = (EditText)view.findViewById(R.id.txtCountry);
        txtCountry.setSingleLine(true);
        txtAddressOne = (EditText)view.findViewById(R.id.txtAddressOne);
        txtAddressOne.setSingleLine(true);
        txtAddressTwo = (EditText)view.findViewById(R.id.txtAddressTwo);
        txtAddressTwo.setSingleLine(true);
        txtCity = (EditText)view.findViewById(R.id.txtCity);
        txtCity.setSingleLine(true);
        txtState = (EditText)view.findViewById(R.id.txtState);
        txtState.setSingleLine(true);
        txtZip = (EditText)view.findViewById(R.id.txtZip);
        txtZip.setSingleLine(true);
        btnAutoFill = (Button)view.findViewById(R.id.btnAutoFill);
        btnAutoFill.setOnClickListener(clkAutoFill);
        btnUseMap = (Button)view.findViewById(R.id.btnUseMap);
        btnUseMap.setOnClickListener(clkUseMap);
        btnUseZip = (Button)view.findViewById(R.id.btnUseZip);
        btnUseZip.setOnClickListener(clkUseZip);
        btnDone = (Button)view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(clkDone);
        apiClient = new GoogleApiClient
                .Builder(act)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        return view;
    }
    View.OnClickListener clkAutoFill = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(locMgr == null){
                locMgr = (LocationManager)act.getSystemService(Context.LOCATION_SERVICE);
            }
            prgDialog = new ProgressDialog(act);
            prgDialog.setMessage("Getting Location...");
            prgDialog.show();
            requestLocation();
        }
    };
    View.OnClickListener clkUseMap = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(apiClient == null || !apiClient.isConnected()){
                Toast.makeText(act, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            }
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            try{
                startActivityForResult(builder.build(act), 0);
            }catch(GooglePlayServicesRepairableException e){
                Toast.makeText(act, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
            }catch( GooglePlayServicesNotAvailableException e ) {
                Toast.makeText(act, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
            }
        }
    };
    View.OnClickListener clkUseZip = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            if(apiClient == null || !apiClient.isConnected()){
                Toast.makeText(act, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
                return;
            }
            new mapByCityStZip().execute();
        }
    };
    public void requestLocation(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(act.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && act.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                return;
            }
        }
        ConnectivityManager conMgr = (ConnectivityManager)act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
        if(netInfo != null){
            if(netInfo.isConnected()) {
                locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locListener);
            }else{
                locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
            }
        }else{
            locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode){
            case 0: //fine location
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocation();
                }
                break;
        }
    }
    LocationListener locListener = new LocationListener(){
        public void onLocationChanged(Location location){
            try {
                Geocoder geocoder = new Geocoder(act, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses.size() > 0) {
                    Address address = addresses.get(0);
                    txtCountry.setText(address.getCountryCode());
                    txtAddressOne.setText(address.getAddressLine(0));
                    txtCity.setText(address.getLocality());
                    txtState.setText(address.getAdminArea().substring(0, 2).toUpperCase());
                    txtZip.setText(address.getPostalCode());
                } else {
                    Toast.makeText(act, "There was an error retrieving your location. Please try again later.", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                Toast.makeText(act, "There was an error retrieving your location. Please try again later.", Toast.LENGTH_SHORT).show();
            } catch (IllegalArgumentException e) {
                Toast.makeText(act, "There was an error retrieving your location. Please try again later.", Toast.LENGTH_SHORT).show();
            }
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (act.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && act.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                    return;
                }
            }
            locMgr.removeUpdates(locListener);
            prgDialog.dismiss();
        }
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
        public void onProviderEnabled(String provider) {
        }
        public void onProviderDisabled(String provider) {
        }
    };
    private class mapByCityStZip extends AsyncTask<Void, Void, Void> {
        ProgressDialog prgDialog = new ProgressDialog(act);
        PlacePicker.IntentBuilder builder;
        @Override
        protected void onPreExecute() {
            prgDialog.setMessage("Fetching location...");
            prgDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Geocoder geocoder = new Geocoder(act);
            try {
                List<Address> addresses = geocoder.getFromLocationName("48154", 1);
                if(addresses.size() > 0) {
                    Address address = addresses.get(0);
                    LatLng latLngOne = new LatLng(address.getLatitude() + .02, address.getLongitude());
                    LatLng latLngTwo = new LatLng(address.getLatitude(), address.getLongitude() + .02);
                    LatLng latLngThree = new LatLng(address.getLatitude() - .02, address.getLongitude());
                    LatLng latLngFour = new LatLng(address.getLatitude(), address.getLongitude() - .02);
                    LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                    boundsBuilder.include(latLngOne);
                    boundsBuilder.include(latLngTwo);
                    boundsBuilder.include(latLngThree);
                    boundsBuilder.include(latLngFour);
                    LatLngBounds bounds = boundsBuilder.build();
                    builder = new PlacePicker.IntentBuilder();
                    builder.setLatLngBounds(bounds);
                }else{
                    Toast.makeText(act, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            try {
                startActivityForResult(builder.build(act), 0);
            } catch (GooglePlayServicesRepairableException e) {
                Toast.makeText(act, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
            } catch (GooglePlayServicesNotAvailableException e) {
                Toast.makeText(act, "There was an error with the map. Please try again later.", Toast.LENGTH_SHORT).show();
            }
            if (prgDialog.isShowing()) {
                prgDialog.dismiss();
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 0 && resultCode == Activity.RESULT_OK){ //place
            Place place = PlacePicker.getPlace(data, act);
            if(place == null) {
                return;
            }
            txtDealerName.setText(place.getName());
            txtPhone.setText(place.getPhoneNumber().toString().replace("+", ""));
            String[] addParts = place.getAddress().toString().split(", ");
            String street;
            String city;
            String[] stateZip;
            String state;
            String zip;
            String country;
            if(addParts.length > 3) {
                street = addParts[0];
                city = addParts[1];
                stateZip = addParts[2].split(" ");
                state = stateZip[0];
                zip = stateZip[1];
                country = addParts[3];
                txtAddressOne.setText(street);
            }else{
                city = addParts[0];
                stateZip = addParts[1].split(" ");
                state = stateZip[0];
                zip = stateZip[1];
                country = addParts[2];
            }
            txtCountry.setText(country);
            txtCity.setText(city);
            txtState.setText(state);
            txtZip.setText(zip);
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public void onStart(){
        super.onStart();
        if(apiClient != null){
            apiClient.connect();
        }
    }
    @Override
    public void onStop(){
        if(apiClient != null && apiClient.isConnected()){
            apiClient.disconnect();
        }
        super.onStop();
    }
    @Override
    public void onConnected(Bundle bundle){
    }
    @Override
    public void onConnectionSuspended(int i){
    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult){
    }
    View.OnClickListener clkDone = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            act.closeFrags();
        }
    };
}
